<?php

namespace Database\Seeders;

use App\Models\User;
use App\Notifications\sendSmsAfterSeed;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\Category::factory(500)->create()->each(function ($category){
            $tags = \App\Models\Tag::factory(rand(2, 8))->make();
            $category->tags()->saveMany($tags);
        });

        $tags = \App\Models\Tag::select('id')->get();
        \App\Models\Post::factory(1000000)->create()->each(function ($post) use ($tags){
            $post->tags()->saveMany($tags->random(rand(2,5)));
        });

//        send sms after finish seeder

        $user = User::first();
        $user->notify(new SendSmsAfterSeed());
    }
}
