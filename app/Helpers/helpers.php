<?php

function maxNumber(array $arr, $max) {
    $first = array_shift($arr);
    for ($i = 0; $i < count($arr); $i++) {
        if ($first + $arr[$i] > $max) {
            return $first . ", " . $arr[$i];
        }
        if ($i + 1 == count($arr)) {
            $first = array_shift($arr);
            $i = -1;
        }
    }
    return "Not found";
}
