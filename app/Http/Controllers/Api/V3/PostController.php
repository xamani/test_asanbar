<?php

namespace App\Http\Controllers\Api\V3;

use App\Http\Controllers\Api\V2\PostController as PostControllerV2;
use App\Http\Resources\V3\PostResource;
use App\Models\Post;

class PostController extends PostControllerV2
{
    public function show(Post $post)
    {
        return new PostResource($post);
    }
}
