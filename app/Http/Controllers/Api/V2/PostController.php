<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Api\V1\PostController as PostControllerV1;
use App\Http\Resources\V2\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends PostControllerV1
{
    public function index(Request $request)
    {
        return PostResource::collection(
            Post::with('tags.category')->simplePaginate($request->input('paginate') ?? 15)
        );
    }
}
