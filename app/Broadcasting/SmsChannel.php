<?php

namespace App\Broadcasting;

use Illuminate\Support\Facades\Notification;

class SmsChannel
{
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toSms($notifiable);

        $message->send();

    }
}
