<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SumNummbers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'number:sum';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'array value is largern than max number';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $arr = $this->ask('Enter number with , between ');
        $max = $this->ask('Enter max number ');
        $validator = \Validator::make(['arr' => $arr, 'max' => $max], [ 'arr' => 'required|string',
            'max'  => 'required|numeric',]);

        if ($validator->fails()) {
            $this->error('input invalid');
            $this->error($validator->messages());
            return null;
        }

        $arr = explode(",", $arr);
        $arr = array_filter($arr, function($value) {
            return !is_null($value) && $value !== '';
        });
        $result = maxNumber($arr , $max);
        $this->info('result: ' . $result);
        $this->info('message run Successfully!');
    }
}
