<?php

namespace App\Notifications;

use App\Broadcasting\SmsChannel;
use App\module\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class SendSmsAfterSeed extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\SmsMessage
     */
    public function toSms($notifiable)
    {
        return (new SmsMessage)
            ->from('Seeder')
            ->to($notifiable->phone)
            ->line("Seeder is done.");
    }

}
