<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function testHundredUsersCreated() {
        $categories = Category::factory(110)->create()->each(function ($category){
            $tags = Tag::factory(rand(2, 8))->make();
            $category->tags()->saveMany($tags);
        });

        $categoryCount = count($categories) >= 100;
        $this->assertTrue($categoryCount);

        $tags = Tag::select('id')->get();
        $post = Post::factory(1000)->create()->each(function ($post) use ($tags){
            $post->tags()->saveMany($tags->random(rand(2,5)));
        });
        $userCount = count($post) >= 1000;

        $this->assertTrue($userCount);
    }

    public function test_can_get_paginated_list_of_all_post()
    {
        Post::factory()->count(25)->create();
;
        $response = $this->get(route('v1.index'));
        $response->assertSuccessful();

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'title',
                    'body',
                    'created_at'
                ]
            ],
            'links' => [
                'first',
                'last',
                'prev',
                'next',
            ],
            'meta' => [
                "current_page",
                "from",
                "path",
                "per_page",
                "to",
            ]
        ]);
    }

    public function test_can_get_a_single_post()
    {
        $post = Post::factory()->create();

        $response = $this->get(route('v3.show', $post));
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'body',
                'created_at'
            ],
        ]);
    }
}
