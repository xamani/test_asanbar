<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::namespace('Api')->group(function () {
    Route::name('v1.')->namespace('V1')->prefix('v1')->group(function () {
        Route::get('posts', 'PostController@index')->name('index');
        Route::get('posts/{post}', 'PostController@show')->name('show');
    });

    Route::name('v2.')->namespace('V2')->prefix('v2')->group(function () {
        Route::get('posts', 'PostController@index')->name('index');
        Route::get('posts/{post}', 'PostController@show')->name('show');
    });

    Route::name('v3.')->namespace('V3')->prefix('v3')->group(function () {
        Route::get('posts', 'PostController@index')->name('index');
        Route::get('posts/{post}', 'PostController@show')->name('show');
    });
});
